const canvas = document.getElementById("ufoCanvas");
const ctx = canvas.getContext("2d");

const DEFAULT_WIDTH = 900;
const DEFAULT_HEIGHT = 700;


const resize = () => {
    const height = window.innerHeight - 50;
    const ratio = DEFAULT_WIDTH / DEFAULT_HEIGHT;
    const width = height * ratio;
    canvas.width = width;
    canvas.height = height;
}

document.addEventListener("load", resize(), false);

class GameBasics {
    constructor(canvas, sounds) {
        this.canvas = canvas;
        this.ctx = canvas.getContext('2d');
        this.width = canvas.width;
        this.height = canvas.height;
        this.playBoundaries = {
            top: 50,
            bottom: 600,
            left: 100,
            right: 800
        };
        this.settings = {
            // FPS 60 Frames per second
            updateSeconds: (1 / 60),

            // Spaceship
            spaceshipSpeed: 400,

            // Bullet
            bulletSpeed: 500,
            bulletMaxRequency: 200,

            // Enemy
            enemyRows: 4,
            enemyColumns: 8,
            enemySpeed: 50,
            enemySinkValue: 50,

            //Bomb
            bombSpeed: 75,
            bombFrequency: 0.05,
        };
        this.positionsContainer = [];
        this.keysPressed = {};
        this.level = 1;
        this.score = 0;
        this.shields = 2;

        this.sounds = sounds;
    };
    presentPosition = () => {
        return this.positionsContainer.length > 0 ? this.positionsContainer[this.positionsContainer.length - 1] : null;
    }
    pushPosition = (position) => {
        this.positionsContainer.push(position);
    }
    popPosition = () => {
        this.positionsContainer.pop();
    }
    goToPosition = (position) => {
        this.isDrawed = false;
        if (this.presentPosition()) {
            this.positionsContainer.length = 0;
        }

        if (position.entry) {
            position.entry(play);
        }
        this.positionsContainer.push(position);
    }
    start = () => {
        play.goToPosition(new OpeningPosition())
        setInterval(() => {
            gameLoop(play)

        }, this.settings.updateSeconds * 1000);
    }
    keyUp = (keyCode) => {
        delete this.keysPressed[keyCode];
    };
    keyDown = (keyCode) => {
        this.keysPressed[keyCode] = true;
        if (play.presentPosition() && play.presentPosition().keyDown) {
            play.presentPosition().keyDown(play, keyCode);
        }
    }
}

const gameLoop = (play) => {
    const presentPosition = play.presentPosition();
    if (presentPosition.draw) {
        presentPosition.draw(play);
    }
    if (presentPosition.update) {
        presentPosition.update(play);
    }

}
const handleKeydown = (e) => {
    const keyCode = e.which || e.keyCode;
    if (keyCode) {
        play.keyDown(keyCode);
    }
}
const handleKeyUp = (e) => {
    const keyCode = e.which || e.keyCode;
    if (keyCode) {
        play.keyUp(keyCode);
    }
}
window.addEventListener('keydown', handleKeydown);
window.addEventListener('keyup', handleKeyUp)
const sounds = new Sounds();
const play = new GameBasics(canvas, sounds);
play.start();