class Enemy {
    constructor(x, y, row, column, image) {
        this.x = x;
        this.y = y;
        this.width = 60;
        this.height = 50;
        this.row = row
        this.column = column;
        this.image = image;
        this.image.src = 'images/enemy.png';
    }
}