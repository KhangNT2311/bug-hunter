class Sounds {

    constructor() {
        this.openingSoundtrack = new Audio('sounds/openingSoundtrack.mp3');
        this.openingSoundtrack.setAttribute("preload", "auto");

        this.ingameSoundtrack = new Audio('sounds/ingameSoundtrack.mp3');
        this.ingameSoundtrack.setAttribute("preload", "auto");

        this.shotSound = new Audio('sounds/shot.mp3');
        this.shotSound.setAttribute("preload", "auto");

        this.explosionSound = new Audio('sounds/explosion.mp3');
        this.explosionSound.setAttribute("preload", "auto");

        this.enemyDeathSound = new Audio('sounds/enemyDeath.mp3');
        this.enemyDeathSound.setAttribute("preload", "auto");

    }
    playSound = (soundName) => {
        switch (soundName) {
            case 'OPENING_SOUNDTRACK':
                this.openingSoundtrack.play();
                this.openingSoundtrack.currentTime = 0;
                break;
            case 'INGAME_SOUNDTRACK':
                this.ingameSoundtrack.play();
                this.ingameSoundtrack.currentTime = 0;
                break;
            case 'SHOT':
                this.shotSound.play();
                this.shotSound.currentTime = 0;
                break;
            case 'ENEMY_DEATH':
                this.enemyDeathSound.play();
                this.enemyDeathSound.currentTime = 0;
                break;
            case 'EXPLOSION':
                this.explosionSound.play();
                this.explosionSound.currentTime = 0;
            default:
                break;
        }
    }
    stopSound = () => {
        this.openingSoundtrack.pause();
        this.openingSoundtrack.currentTime = 0;
        this.shotSound.pause();
        this.shotSound.currentTime = 0;
        this.enemyDeathSound.pause();
        this.enemyDeathSound.currentTime = 0;
        this.explosionSound.pause();
        this.explosionSound.currentTime = 0;
    }
}