class SpaceShip {
    constructor(x, y, image) {
        this.x = x;
        this.y = y;
        this.width = 34;
        this.height = 28;
        this.image = image;
        this.image.src = "images/ship.png";
    }
}