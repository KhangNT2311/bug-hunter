
class OpeningPosition {
    entry = (play) => {
        // play.sounds.stopSound();
        play.sounds.playSound('OPENING_SOUNDTRACK');
    }
    draw = (play) => {
        const { ctx } = play;
        ctx.clearRect(0, 0, play.width, play.height);
        // Create UFO Hunter text 
        const gradient = ctx.createLinearGradient(play.width / 2 - 180, play.height / 2, play.width / 2 + 180, play.height / 2);
        gradient.addColorStop(0, 'yellow');
        gradient.addColorStop(.5, 'red');
        gradient.addColorStop(1, 'yellow');
        ctx.fillStyle = gradient;
        ctx.textAlign = "center";
        ctx.font = "90px Comic Sans MS";
        ctx.fillText("Bug Hunter", play.width / 2, play.height / 2 - 160);


        // Create Press 'Space' to start
        ctx.font = "40px Comic Sans MS";
        ctx.fillStyle = 'yellow';
        ctx.fillText("Press 'Space' To Start", play.width / 2, play.height / 2 - 40);

        // Create instruction
        ctx.fillStyle = 'white';
        ctx.font = "50px Comic Sans MS";
        ctx.fillText('Game Controls', play.width / 2, play.height / 2 + 60);
        ctx.font = "30px Comic Sans MS";
        ctx.fillText('Left arrow: Move left', play.width / 2, play.height / 2 + 140);
        ctx.fillText('Right arrow: Move right', play.width / 2, play.height / 2 + 200);
        ctx.fillText('Space: Fire', play.width / 2, play.height / 2 + 260);
    }
    keyDown = (play, keyCode) => {
        if (keyCode === 32) {
            play.goToPosition(new TransferPosition(play.level))
        }
    }
}