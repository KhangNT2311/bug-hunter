class InGamePosition {
    constructor(settings, level, sounds) {
        this.settings = settings;
        this.level = level;
        this.sounds = sounds;
        this.spaceship = null;
        this.bullets = [];
        this.lastTimeShoot = null;
        this.enemies = [];
        this.turnAround = 1;
        this.isHorizontalMove = true;
        this.sinkValue = 0;
        this.bombs = [];
        this.bombSpeed = 0;
        this.bombChance = 0;
    }
    draw = (play) => {
        const { ctx, width, height } = play;
        ctx.clearRect(0, 0, width, height);
        ctx.drawImage(this.spaceship.image, this.spaceship.x - (this.spaceship.width / 2), this.spaceship.y - (this.spaceship.height / 2));
        this.bullets.forEach((bullet, index) => {
            ctx.fillStyle = 'red';
            ctx.textAlign = "center";
            ctx.fillRect(bullet.x - 3, bullet.y - 6, 3, 8);

        })
        this.enemies.forEach((enemy) => {
            ctx.textAlign = "center";
            ctx.drawImage(enemy.image, enemy.x - (enemy.width / 2), enemy.y - (enemy.height / 2), enemy.width, enemy.height);
        })
        ctx.fillStyle = "pink";
        this.bombs.forEach((bomb, index) => {
            ctx.fillRect(bomb.x - 2, bomb.y + 5, 4, 6);

        })
    }
    checkIsReachedBoundary = (play) => {
        this.enemies.forEach((enemy) => {
            const move = Math.round(this.settings.enemySpeed * this.settings.updateSeconds * this.level * this.turnAround);
            let x = enemy.x + move;
            if (x <= play.playBoundaries.left || x >= play.playBoundaries.right) {
                this.turnAround *= -1;
                this.isHorizontalMove = false;
                return true;
            }
        })
        return false;
    }
    update = (play) => {
        if (play.keysPressed[37] && this.spaceship.x > play.playBoundaries.left) {
            this.spaceship.x -= this.settings.spaceshipSpeed * this.settings.updateSeconds;
        }
        if (play.keysPressed[39] && this.spaceship.x < play.width - 100) {
            this.spaceship.x += this.settings.spaceshipSpeed * this.settings.updateSeconds;
        }
        if (play.keysPressed[32]) {
            this.shoot();
        }
        this.checkIsReachedBoundary(play);

        // animate bullet
        this.bullets.forEach((bullet, index) => {
            bullet.y = bullet.y - this.settings.bulletSpeed * this.settings.updateSeconds;
            if (bullet.y < play.playBoundaries.top) {
                this.bullets.splice(index--, 1);
            }
        })

        this.enemies.forEach((enemy) => {
            if (this.isHorizontalMove) {
                const move = Math.round(this.settings.enemySpeed * this.settings.updateSeconds * this.level * this.turnAround);
                let x = enemy.x + move;
                enemy.x = x;
            }
            else {
                let y = enemy.y + Math.round(this.settings.enemySpeed * this.settings.updateSeconds);
                enemy.y = y;
            }
        });
        if (!this.isHorizontalMove) {
            const move = Math.round(this.settings.enemySpeed * this.settings.updateSeconds);
            this.sinkValue += move;
            if (this.sinkValue >= this.settings.enemySinkValue) {
                this.isHorizontalMove = true;
                this.sinkValue = 0;
            }
        }
        let frontEnemies = [];
        this.enemies.forEach((enemy) => {
            if (!frontEnemies[enemy] || frontEnemies[enemy].row < enemy.row) {

                frontEnemies[enemy.column] = enemy;
            }
        })

        frontEnemies.forEach((frontEnemy) => {
            const random = Math.random();
            if (random < this.bombChance * this.settings.updateSeconds) {
                const bomb = new Bomb(frontEnemy.x, frontEnemy.y);
                this.bombs.push(bomb);
            }
        })

        this.bombs.forEach((bomb, index) => {
            bomb.y += this.bombSpeed * this.settings.updateSeconds;
            if (bomb.y > this.height) {
                this.bombs.splice(index--, 1);
            }
        })

        // enemy collision
        let collision = false;
        this.enemies.forEach((enemy, enemyIndex) => {
            this.bullets.forEach((bullet, bulletIndex) => {
                if (bullet.x + 1 >= (enemy.x - enemy.width / 2) && bullet.x - 1 <= (enemy.x + enemy.width / 2) && bullet.y + 8 <= (enemy.y + enemy.height / 2) && bullet.y >= (enemy.y - enemy.height / 2)) {
                    collision = true;
                    this.bullets.splice(bulletIndex--, 1);
                }
            })
            if (collision) {
                this.enemies.splice(enemyIndex--, 1);
                collision = false;
                this.sounds.playSound('ENEMY_DEATH')
            }
        })

        // spaceship collision
        this.bombs.forEach((bomb, index) => {
            if (bomb.x + 1 >= (this.spaceship.x - this.spaceship.width / 2) && bomb.x - 1 <= (this.spaceship.x + (this.spaceship.width / 2)) && bomb.y >= (this.spaceship.y - this.spaceship.height / 2) && bomb.y - 6 <= (this.spaceship.y + this.spaceship.height / 2)) {
                this.bombs.splice(index--, 1);
                this.sounds.playSound('EXPLOSION')

            }
        })
    }
    entry = (play) => {
        const { width, height, settings, sounds } = play;
        sounds.stopSound();
        sounds.playSound('INGAME_SOUNDTRACK');
        const image = new Image();
        const enemyImage = new Image();
        const spaceship = new SpaceShip((width / 2 - 34), (height - 56), image);
        this.spaceship = spaceship;

        // init bomb
        this.bombSpeed = settings.bombSpeed + (this.level * 10);
        this.bombChance = settings.bombFrequency + (this.level * 0.05);


        // Create enemies
        for (let row = 0; row < this.settings.enemyRows; row++) {
            for (let column = 0; column < this.settings.enemyColumns; column++) {
                const x = play.width / 2 + (column * 80) - 300;
                const y = play.playBoundaries.top + row * 30;
                const enemy = new Enemy(x, y, row, column, enemyImage);
                this.enemies.push(enemy);
            }
        }

    }
    shoot = () => {
        const now = Date.now();
        if (!this.lastTimeShoot || (now - this.lastTimeShoot >= this.settings.bulletMaxRequency)) {
            const bullet = new Bullet(this.spaceship.x, this.spaceship.y - this.spaceship.height / 2);
            this.lastTimeShoot = now;
            this.bullets.push(bullet);
            this.sounds.playSound('SHOT');
        }
    }
    keyDown = (play, keyCode) => {

    }

}