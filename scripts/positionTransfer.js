class TransferPosition {
    constructor(level) {
        this.level = level;
        this.fontSize = 140;
        this.color = 255;
    }
    draw = (play) => {
        const { ctx, width, height } = play;
        ctx.clearRect(0, 0, width, height);
        ctx.font = `140px Arial`;
        ctx.fillText(`Get Ready With Level ${this.level}`, play.width / 2, play.height / 2);
    }
    update = (play) => {
        const { ctx, width, height } = play;
        this.fontSize--;
        this.color -= 2;
        ctx.clearRect(0, 0, width, height);
        ctx.font = `${this.fontSize}px Arial`;
        ctx.fillStyle = `rgba(255,${this.color},${this.color},1)`
        ctx.fillText(`Get Ready With Level ${this.level}`, play.width / 2, play.height / 2);

        if (this.fontSize === 0) {
            play.goToPosition(new InGamePosition(play.settings, this.level,play.sounds))
        }
    }
}